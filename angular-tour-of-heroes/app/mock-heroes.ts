import { Hero } from './hero';
 export const HEROES:Hero[] = [
  {id:1,name:'Roshan Sharma'},
  {id:2,name:'Subash Prabhat'},
  {id:3,name:"Anil Adhikari"},
  {id:4,name:"Sujit Khatiwada"},
  {id:5,name:"Prasant Bhate"},
  {id:6,name:"Keshav Ghimire"},
  {id:7,name:"Subash Kharel"},
  {id:8,name:"Ravi Dhakal"},
  {id:9,name:"Rakesh Shrestha"}];
