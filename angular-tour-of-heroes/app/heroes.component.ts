import { Component, OnInit } from '@angular/core';
import { Hero } from './hero';
import { HeroService } from './hero.service';
import { Router } from '@angular/router';
@Component({
  moduleId:module.id,
  templateUrl:'heroes.component.html', 
  styleUrls:['heroes.component.css']

})
export class HeroesComponent implements OnInit{
title ="Tour of heroes";
selectedHero:Hero;
heroes:Hero[];

constructor(
private heroservice:HeroService,
private router:Router
){}

onSelect(hero:Hero): void{
  this.selectedHero=hero;
}

ngOnInit(): void{
  this.getHeroes();
}
 
 getHeroes(): void{
 this.heroservice.getHeroes().then(heroes=>this.heroes=heroes);
 }
 gotoDetail(): void{
 this.router.navigate(['/detail',this.selectedHero.id]);
 }

 add(name: string): void{
 name=name.trim();
 if(!name) {return;}
 this.heroservice.create(name).then(hero=>{
 this.heroes.push(hero);
 this.selectedHero=null;
 });
}
 delete(hero:Hero): void {
 this.heroservice.delete(hero.id).then(()=>{
 this.heroes=this.heroes.filter(h=>h!==hero);
 if(this.selectedHero===hero){
 this.selectedHero=null;
 }
 });
 }

}
