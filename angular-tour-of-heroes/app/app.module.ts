import { NgModule }      from '@angular/core';
import { BrowserModule} from '@angular/platform-browser';
import { FormsModule }  from '@angular/forms';
import { HttpModule }  from '@angular/http';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

import { InMemoryDataService } from './in-memory-data.service';


import { AppComponent }   from './app.component';
import { HeroesComponent }   from './heroes.component';
import { HeroDetailComponent} from './hero-detail.component';
import { HeroService } from './hero.service';
import { DashboardComponent } from './dashboard.component';
import { HeroSearchComponent } from './hero-search.component';
import { AppRoutingComponent } from './app-routing.component';

import './rxjs-extensions';

@NgModule({
  declarations: [ 
                  AppComponent, 
                  HeroDetailComponent,
                  HeroesComponent,
                  DashboardComponent,
                  HeroSearchComponent
                  ],
  bootstrap:    [ AppComponent ],
  imports: [
             BrowserModule,
             FormsModule,
             AppRoutingComponent,
             HttpModule,
             InMemoryWebApiModule.forRoot(InMemoryDataService)
             ],
  providers:[HeroService]
})
export class AppModule { }
